var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
      res.render('tbltransaction', { title: 'T-BASE History transaction' });
});

module.exports = router;
